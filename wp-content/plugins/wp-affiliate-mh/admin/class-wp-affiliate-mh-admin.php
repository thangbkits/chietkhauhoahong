<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://dominhhai.com
 * @since      1.0.0
 *
 * @package    Wp_Affiliate_Mh
 * @subpackage Wp_Affiliate_Mh/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Wp_Affiliate_Mh
 * @subpackage Wp_Affiliate_Mh/admin
 * @author     Đỗ Minh Hải <minhhai27121994@gmail.com>
 */
class Wp_Affiliate_Mh_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

		new AFF_Ajax_Admin();

		add_action('admin_menu', [$this, 'add_admin_pages']);

		add_filter( 'wp_mail_content_type', function(){
			return "text/html";
		});


		add_action('wp_trash_post', [$this, 'woocommerce_trash_order']);
	}

	public function woocommerce_trash_order($post_id){
		AFF_User_Order::moveOrderToTrash($post_id);
	}

	public function add_admin_pages()
	{
		
			$icon = AFF_URL . 'public/images/box.svg';
			add_menu_page(
		        __( 'WP Affiliate MH', 'textdomain' ),
		        'WP Affiliate MH',
		        'manage_options',
		        'wp-affiliate-mh',
		        [$this, 'admin_template'],
		        $icon,
		        110
		    );

		    
	}

	public function admin_template()
	{
		// wp_redirect( AFF_URL .'admin/' );
		require_once plugin_dir_path( __FILE__ ) . 'partials/' .$this->plugin_name . '-admin-display.php';
	}
	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Wp_Affiliate_Mh_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Wp_Affiliate_Mh_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/wp-affiliate-mh-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Wp_Affiliate_Mh_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Wp_Affiliate_Mh_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/wp-affiliate-mh-admin.js', array( 'jquery' ), $this->version, false );

	}

}
