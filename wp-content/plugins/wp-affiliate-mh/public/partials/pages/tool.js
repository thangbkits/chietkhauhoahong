

const template = `
<div class="q-pa-md">
    <div>
        
        <div class="row q-col-gutter-md row-eq">
            <div class="col-md-6 col-12">
                <div class="card-item">
                    <q-chip icon="event">Tạo URL giới thiệu</q-chip>
                    <div class="flex q-mt-md space-bw">
                        <q-input filled v-model="url" label="Nhập URL cần tạo đường dẫn giới thiệu" style="width:80%" dense/>
                        <q-btn color="primary" style="width:18%;" @click="createAffUrl" size="sm">Tạo URL</q-btn>
                        <p class="q-mt-md">
                            Bạn có thể sao chép URL sản phẩm, URL danh mục để tạo
                            đường dẫn giới thiệu cho người mua
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-12">
                <div class="card-item" v-if="Object.keys(settings).length && !isNormal()">
                    <q-chip icon="event">Chia sẻ URL đăng kí cho cộng tác viên</q-chip>
                    <div class="flex q-mt-md space-bw">
                        <q-input filled v-model="register_link"  style="width:80%" dense />
                        <q-btn color="primary" style="width:18%;" @click="copyRegisterLink" size="sm">COPY</q-btn>
                        <p class="q-mt-md">
                            Bạn có thể tuyển cộng tác viên cấp dưới theo URL này
                        </p>
                    </div>
                </div>
            </div>
        </div>
            
    </div>
</div>
`;

const { RV_CONFIGS } = window 
export default {
    data: () => ({
        configs: RV_CONFIGS,
        isLoading: true,
        settings: {},
        url: '',
        register_link: ''

    }),
   
    methods: {
       createAffUrl() {
        if (this.url == "") {
            this.$notify("URL không thể để trống", 0);
            return;
        }

        const final_url =
            this.url + "?ref=" + this.configs.user_login;
        this.$q.dialog({
                title: "Chia sẻ link giới thiệu",
                message: final_url,
                cancel: false,
                textOk: "Coppy",
                persistent: true
            })
            .onOk(() => {
                Quasar.utils.copyToClipboard(sc)
            });
        },
        copyRegisterLink(){
            Quasar.utils.copyToClipboard(this.register_link)
            this.NOTIFY('Sao chép thành công')
        }
	},
	components:{
       
	},
    watch:{

    },
    template: template,
    created(){
         this.getConfigs().then(res => {
            this.settings = res
            this.register_link = this.settings.aff_user_page + '/#/dang-ky?ref=' + this.configs.user_login
        })
        this.$eventBus.$emit('set.page_title', 'Công cụ hỗ trợ');
    }

}